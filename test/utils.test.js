import { describe, expect, test } from 'vitest'
import { compareObjects, clearVoidFilters } from './src/utils.js'

import objlist from './data/objects_compare.json'
const objects = objlist.objects
const filters = objlist.filters.map(e => clearVoidFilters(e))

function nantonan(obj) {
	for (const k of Object.keys(obj)) {
		if (obj[k] === "NaN")
			obj[k] = NaN
		else if (typeof obj[k] === 'object')
			nantonan(obj[k])
	}
	return obj
}

describe('compareObjects', () => {
  test('filters vides', () => {
		const result = compareObjects(nantonan(filters[0]), objects[0])
    expect(result).toBe(true)
  })
  test('select nom dans liste de string', () => {
		const result = compareObjects(nantonan(filters[1]), objects[0])
    expect(result).toBe(true)
  })
  test('select nom semblables dans une liste de string', () => {
		const result = compareObjects(nantonan(filters[1]), objects[3])
    expect(result).toBe(false)
  })
  test('object 1 / filter 4', () => {
		const result = compareObjects(nantonan(filters[3]), objects[0])
    expect(result).toBe(false)
  })

  test('4 valid ranges', () => {
		const result = compareObjects(nantonan(filters[4]), objects[3])
    expect(result).toBe(true)
  })

  test('1 valid range, 2 invalid', () => {
		const result = compareObjects(nantonan(filters[5]), objects[3])
    expect(result).toBe(false)
  })
})

describe('clearVoidFilters', () => {
  test('retour nul si filtres vides/par défaut', () => {
		const result = clearVoidFilters(filters[0])
		expect(result).toStrictEqual({})
  })
  test('un seul filtre non nul, type string', () => {
		const result = clearVoidFilters(filters[1])
		expect(result).toStrictEqual({nom: ['Objet 1', 'Objet 2', 'Objet']})
  })
  test('2 filtres non nuls, type number', () => {
		const result = clearVoidFilters(filters[3])
		expect(result).toStrictEqual({details: {poids: {min: 0, max: 20}, dimensions: {profondeur: {min: 10, max: 20}}}})
  })
  test('un filtre non nul type number, un type string', () => {
		const result = clearVoidFilters(nantonan(filters[2]))
		expect(result).toStrictEqual({nom: ['Objet 2'], details: {poids: {min: 0, max: 9}}})
  })
})
