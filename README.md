# switchselector

Filter a list of network switches  

Accessible thanks to Gitlab Pages at https://lpoujade.frama.io/switchselector


## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

> add `-- --base='/subfoldername'` if you plan to host it under /subfoldername

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

### Run tests

```sh
npm run test
```

