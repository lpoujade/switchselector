
code organisation
---

This project consists of 2 main components: a form with the filters (`SwitchFilters`), and a list of results rendered using `SwitchItem`  
The top level App component creates a `defaultFilter` object (a manual copy of the items structure with field types, see below), and pass it to the `SwitchFilters` component which creates the necessary fields. The `SwitchFilters` component also get the full items list to extracts extents for range fields and existing values for string choice lists, and a `filters` object which will be used to transmits the currently selected filters  

The `SwitchFilters` component emits to its parent when the filter list is modified, and then the parent render the filtered list of objects

default json desc
---

{\_obj: true} indicate type is object, to recurse into  
[] is an array of string like brands (shows all objects which matches with at least one of selected string)  
{min: NaN, max: NaN} is for numbers  
bool for simple properties  


components
---

SwitchFilters
-----
Render a `<form>` containing a list of `FieldSet`


FieldSet
-----
Render a `<fieldset>` with a list of `<input>` or `FieldSet`, accordingly to the filters in the `modelValue` which serves at the same time as form template and as filters state


RangeSelector
-----
Custom range input using
- two inputs of type `number` to select minimum/maximum values
- a reset `<button>` to clear and disable this filter
- one input of type `checkbox` to toggle filtering on this property whithout clearing the selected values


SwitchItem
-----
Render a list of `DataList`


DataList
-----
Render a list of keys/values list using `<dt>/<dd>` elements, or a `DataList`. Each list of keys/values are rendered according to its type (bools, strings, and numbers are handled)
