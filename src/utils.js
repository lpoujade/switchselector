
/* compare an object with a ref object, and compare:
 * min/max for numbers
 * equality for true bools
 * if the value is in the ref array for the strings
 */
export function compareObjects(ref, obj) {
	let subprops = []
	for (let k of Object.keys(ref)) {
		if (!Object.prototype.hasOwnProperty.call(obj, k))
			return false

		if (Object.hasOwnProperty.call(ref[k], 'min') && Object.hasOwnProperty.call(ref[k], 'max')) {
				let r = obj[k] >= ref[k].min && obj[k] <= ref[k].max
				if (!r) return false
		} else if (Array.isArray(ref[k])) {
			if (ref[k].length === 0) continue
			if (!ref[k].find(el => el === obj[k]))
				return false
		} else if (typeof ref[k] === 'boolean') {
			if (ref[k] && ref[k] !== obj[k])
				return false
		} else if (typeof ref[k] === 'object') {
			subprops.push(k)
		}
	}
	for (let key of subprops) {
		if (!compareObjects(ref[key], obj[key]))
			return false
	}
	return true
}

export function clearMetadata(ref) {
	let cleanedfilters = {}
	for (let k of Object.keys(ref)) {
		if (k === '_obj') continue
		if (Array.isArray(ref[k]))
			cleanedfilters[k] = ref[k]
		else if (typeof ref[k] === 'object') {
			let sub_k = clearMetadata(ref[k])
			if (Object.keys(sub_k).length > 0)
				cleanedfilters[k] = sub_k
		} else
			cleanedfilters[k] = ref[k]
	}
	return cleanedfilters
}

/* return a new object without properties which have the default value */
export function clearVoidFilters(ref) {
	let cleanedfilters = {}
	for (let k of Object.keys(ref)) {
		if (Object.hasOwnProperty.call(ref[k], 'min') && Object.hasOwnProperty.call(ref[k], 'max')) {
			if (!isNaN(ref[k].min) && !isNaN(ref[k].max)) cleanedfilters[k] = ref[k]
		} else if (Array.isArray(ref[k])) {
			if (ref[k].length > 0) cleanedfilters[k] = ref[k]
		} else if (ref[k] === true) {
			cleanedfilters[k] = ref[k]
		} else if (typeof ref[k] === 'object') {
			let sub_k = clearVoidFilters(ref[k])
			if (Object.keys(sub_k).length > 0)
				cleanedfilters[k] = sub_k
		}
	}
	return cleanedfilters
}

/* (obj={a: {b: {c: 1, d: 3}}}, str='a.b.d') -> obj[a][b][d] */
export function getValueFromNestedProperty(obj, str) {
	if (obj === undefined) return undefined
	if (str == undefined) return obj
	const keys = str.split('.');
	let value = obj;

	for (let key of keys) {
		if (Object.prototype.hasOwnProperty.call(value, key)) {
			value = value[key];
		} else return undefined;
	}
	return value;
}
